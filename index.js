let express = require("express");
	
const PORT = 3000;
	
let app = express();
	
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.get("/home",(req,res)=> res.send(`Welcome to the home page`));

app.post("/signup",(req,res)=> {
	let username = req.body.username;
	let password = req.body.password;	
	    if(username.length == 0 && password == 0){
	        res.send(`Please input BOTH username and password.`)
	    }else{
	     	res.send(`User ${req.body.username} successfully registered!`)
	        }
});                

app.listen(PORT, ()=> {
	console.log(`Server running at port ${PORT}`)
});

